#!/usr/bin/env python3

"""Routines that manipulate an image and return the manipulated image

Three manipulations are provided:

Rotation:  Rotation can be performed only in 90-degree increments.  For JPEG
images, rotation is performed using exiftran if it is available.  If exiftran
is not available or the image is not JPEG, PILLOW image rotation is used.

Cropping:  Cropping is done using PILLOW cropping.  The function requires the
image and the coordinates of the crop box.

Scaling:  Scaling is done using PILLOW scaling.  The function requires the
image and the fractional scaling amount (0. to 1.).

The script requires the packages Image and ImageTk from the Python Imaging
Library as well as the package Tkinter that provides python bindings for the
Tk gui toolkit.  For saving metadata files with the images, the PyYAML package
is required, along with the other packages listed in the 'if yaml_found' code
block.

Copyright (c) 2020 Norman Wood. All rights reserved.
normw013@fastmail.fm

SPDX-License-Identifier:  BSD-3-Clause
https://spdx.org/licenses/BSD-3-Clause.html#licenseText
"""

import PIL.Image
import subprocess
import tempfile

def scale_image(image, scale_factor):
    newsize = (int(image.size[0]*scale_factor), int(image.size[1]*scale_factor))
    scaled_image = image.resize(newsize, PIL.Image.LANCZOS)
    return scaled_image

#Basic image rotation using PILLOW
def rotate_image(image):
    rotated_image = image.rotate(90, expand=True)
    return rotated_image

def rotate_image_exiftran(image):
    #If the source file is a jpeg, try to apply exiftran to do the rotation losslessly
    #The problem is that exiftran doesn't operate as a filter.  It requires a file from
    #which to read in and to which to write out the image.
    #Need to create a tmp file
    #Get a tempfile to which to save the image so that exiftran can work on it
    image_buffer = tempfile.NamedTemporaryFile()
    #Saving a JPEG without loss requires 100% quality and no chroma subsampling
    image.save(image_buffer.name, format='JPEG', subsampling=0, quality=100)
    #Use exiftran to rotate the image in place
    command = ['exiftran', '-9', '-i', image_buffer.name]
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    rotated_image = PIL.Image.open(image_buffer.name)
    image_buffer.close()
    return rotated_image

def crop_image(image, cropbox_coords_start, cropbox_coords_end):
    cropped_image = image.crop((int(cropbox_coords_start[0]), int(cropbox_coords_start[1]),
                                int(cropbox_coords_end[0]), int(cropbox_coords_end[1])))
    return cropped_image

class CropBox():
    def __init__ (self):
        self.coords_start = [None, None]
        self.coords_end = [None, None]

if __name__ == '__main__':
    import sys
    image = PIL.Image.open(sys.argv[1])
    rotated_image = rotate_image_exiftran(image)
    rotated_image.save('dump_rotated.jpg')
    cropbox = True
    cropbox.coords_start[0] = 0
    cropbox.coords_start[1] = 0
    cropbox.coords_end[0] = int(0.5*rotated_image.size[0])
    cropbox.coords_end[1] = int(0.5*rotated_image.size[1])
    cropped_image = crop_image(rotated_image, cropbox_coords_start, cropbox_coords_end)
    cropped_image.save('dump_rotated_cropped.jpg')

    scaled_image = scale_image(cropped_image, 0.5)
    scaled_image.save('dump_rotated_cropped_scaled.jpg')
