#!/usr/bin/env python3

"""RImRoCS:  Raster Image Rotate-Crop-Scale

This is a Python tkinter-based GUI tool for displaying and editing (scale,
crop and rotate) raster images.  It uses the Python Imaging Library (PILLOW)
and exiftran if available to display and modify images.  When saving the 
modified image, it can also save a YAML-formatted file containing basic
image metadata (sometimes called a 'sidecar file') along with information
about the source image from which the saved image was derived.

Copyright (c) 2020 Norman Wood. All rights reserved.
normw013@fastmail.fm

SPDX-License-Identifier:  BSD-3-Clause
https://spdx.org/licenses/BSD-3-Clause.html#licenseText
"""

import sys
import os
import PIL.Image
import PIL.ImageTk
import tkinter
import tkinter.filedialog
import time
import subprocess
import collections
import configparser
import distutils.spawn

from . import meta
from . import manipulate


#Check for other support needed to handle image metadata and enable metadata
#handling only if support is available.
import datetime
import hashlib

#Check if exiftran is available for jpeg rotations
if distutils.spawn.find_executable('exiftran') is not None:
    _ENABLE_EXIFTRAN = True
else:
    _ENABLE_EXIFTRAN = False


class ImgDisplay():

    #Scaling an image up and down repeatedly can cause degradation of the image.  To
    #avoid this, a working_image is saved.  This working_image is the current representation
    #of the source image, with any cropping or rotations applied, but at full scale.
    #The displayed_image is the working_image with the current scale factor applied and
    #displayed on the canvas.  Any time the scale factor is changed, the working_image is
    #accessed and scaled to this value. The displayed_image is the image that will be saved
    #if the 'Save' or 'Save w/ Meta' buttons are pressed.

    def __init__(self):
        self.image_fpaths = []
        self.image_index = 0
        self.image_format = None
        self.image_scale_factor = 1.
        self.image_size_orig = None
        self.working_image = None
        self.displayed_image = None
        self.dwell_time = 1.
        self.log_fptr = None
        self.slideshow_running = False
        self.root = tkinter.Tk()
        self.image_for_canvas = None
        self.cropbox = None
        self.cropbox_coords_start = None
        self.cropbox_coord_end = None

        if _ENABLE_EXIFTRAN:
            self.enable_exiftran = True
        else:
            self.enable_exiftran = False

        #Add a frame on the bottome for buttons for basic functions
        self.buttonframe = tkinter.Frame(self.root)
        self.buttonframe.pack(side='bottom')

        self.btn_quit = tkinter.Button(self.buttonframe,
                                       text='Quit',
                                       command=self.quit)
        self.btn_quit.pack(side='left', padx=(10,60))

        self.btn_rotate = tkinter.Button(self.buttonframe,
                                         text='Rotate 90CW',
                                         command=self.rotate_working_image)
        self.btn_rotate.pack(side='left')

        self.btn_crop = tkinter.Button(self.buttonframe,
                                       text='Crop',
                                       command=self.crop_working_image)
        self.btn_crop.pack(side='left')
        self.btn_crop.config(state=tkinter.DISABLED)

        self.btn_clear_cropbox = tkinter.Button(self.buttonframe,
                                                text='Clear Crop',
                                                command=self.clear_cropbox)
        self.btn_clear_cropbox.pack(side='left')
        self.btn_clear_cropbox.config(state=tkinter.DISABLED)

        self.btn_load = tkinter.Button(self.buttonframe,
                                       text='Reload',
                                       command=lambda s=self: s.reload())
        self.btn_load.pack(side='left', padx=(30,0))

        self.btn_prev = tkinter.Button(self.buttonframe,
                                       text='Previous',
                                       command=lambda s=self: s.display(seq='prev'))
        self.btn_prev.pack(side='left')


        self.btn_next = tkinter.Button(self.buttonframe,
                                       text='Next',
                                       command=lambda s=self: s.display(seq='next'))
        self.btn_next.pack(side='left')

        self.btn_save = tkinter.Button(self.buttonframe,
                                       text='Save',
                                       command=self.save_image)
        self.btn_save.pack(side='left', padx=(30,0))

        self.btn_save_w_meta = tkinter.Button(self.buttonframe,
                                              text='Save w/ Meta',
                                              command=self.save_image_with_meta)
        self.btn_save_w_meta.pack(side='left')
        if not meta._ENABLE_META:
            self.btn_save_w_meta.config(state=tkinter.DISABLED)

        #Include a small label with very small copyright statement
        self.copyright_label = tkinter.Label(self.buttonframe,
                                             text = 'Copyright ' + u"\u00A9" +  '2020\nNorman Wood',
                                             justify = tkinter.CENTER,
                                             anchor=tkinter.E,
                                             font=('Helvetica', 8))
        self.copyright_label.pack(anchor=tkinter.E, side='right')

        self.btn_slideshow = tkinter.Button(self.buttonframe,
                                            text='Slideshow',
                                            command=self.slideshow)
        self.btn_slideshow.pack(side='right',padx=60, anchor=tkinter.E)



        #Add frames on the right to contain sliders to control image scaling
        #and dwell time for slideshows
        self.scaleframe = tkinter.Frame(self.root)
        self.scaleframe.pack(side='right')
        self.scale_label = tkinter.Label(self.scaleframe,
                                         text= 'Scale\n',
                                         font=('Helvetica', 12),
                                         justify=tkinter.CENTER)
        self.scale_label.pack(anchor=tkinter.N, side='top')
        self.scale_size = tkinter.Scale(self.scaleframe,
                                        from_=0,
                                        to=1.0,
                                        resolution=0.01,
                                        orient=tkinter.VERTICAL,
                                        command=self.adjust_image_scale,
                                        length=400,
                                        width=30)
        self.scale_size.set(self.image_scale_factor)
        self.scale_label.pack(anchor=tkinter.CENTER, side='top')
        self.scale_size.pack(anchor=tkinter.CENTER)


        self.dwellframe = tkinter.Frame(self.root)
        self.dwellframe.pack(side='right')
        self.dwell_label = tkinter.Label(self.dwellframe,
                                         text = 'Slideshow\nDelay',
                                         font= ('Helvectica', 12),
                                         justify=tkinter.CENTER)
        self.scale_dwell = tkinter.Scale(self.dwellframe,
                                         from_=0.05,
                                         to=2.5,
                                         resolution=0.1,
                                         orient=tkinter.VERTICAL,
                                         command=self.set_dwell_time,
                                         length=400, 
                                         width=30)
        self.scale_dwell.set(self.dwell_time)
        self.dwell_label.pack(anchor=tkinter.CENTER, side='top')
        self.scale_dwell.pack(anchor=tkinter.CENTER)


        #Add a frame on the top right to contain title
        self.titleframe = tkinter.Frame(self.root, width=50, height=25)
        self.title_label = tkinter.Label(self.titleframe,
                                               text = 'RImRoCS: Raster Image Rotate-Crop-Scale',
                                               justify = tkinter.CENTER,
                                               anchor=tkinter.N,
                                               font=('Helvetica', 16))
        self.title_label.pack(anchor=tkinter.CENTER, side='top')
        self.titleframe.pack(anchor=tkinter.E,side='top', fill=tkinter.X)



        #Add a frame in the remaining space to contain a canvas with scrolling to contain
        #the displayed image
        self.image_frame = tkinter.Frame(self.root)
        self.image_frame.grid_rowconfigure(0, weight=1)
        self.image_frame.grid_columnconfigure(0, weight=1)
        self.xscroll = tkinter.Scrollbar(self.image_frame, orient=tkinter.HORIZONTAL)
        self.xscroll.grid(row=1, column=0, sticky=tkinter.EW)
        self.yscroll = tkinter.Scrollbar(self.image_frame, orient=tkinter.VERTICAL)
        self.yscroll.grid(row=0, column=1, sticky=tkinter.NS)
        self.canvas = tkinter.Canvas(self.image_frame, xscrollcommand=self.xscroll.set, yscrollcommand=self.yscroll.set, borderwidth=15, cursor='tcross')
        self.canvas.grid(row=0, column=0, sticky=tkinter.N+tkinter.S+tkinter.E+tkinter.W)
        self.xscroll.config(command=self.canvas.xview)
        self.yscroll.config(command=self.canvas.yview)
        self.image_frame.pack(fill=tkinter.BOTH, expand=1)


        #Mouse button bindings for setting up cropping region
        #Bind left mouse button press to get coordinates
        self.canvas.bind('<Button-1>', self.cropbox_start)
        #Bind a move event with left mouse button pressed
        self.canvas.bind('<B1-Motion>', self.cropbox_make)
        #Bind left mouse button release to set up crop region
        self.canvas.bind('<ButtonRelease-1>', self.cropbox_end)

        #Mouse button bindings for logging the current source file name
        #Bind right mouse button to log the source file name
        self.root.bind('<Button-3>', self.write_fpath)

    def read_config(self, fpath_config=None):

        Config = collections.namedtuple('Config', ['initial_geometry',
                                                   'initial_scale_factor',
                                                   'initial_slideshow_dwelltime',
                                                   'logfile_path'])

        #Set some configurable options for the GUI
        if fpath_config is None:
            fpath_config = './rimrocs.cfg'

        parser = configparser.ConfigParser(allow_no_value=True, interpolation=None)

        initial_geometry_string = None
        initial_scale_factor = None
        initial_slideshow_dwelltime = None
        logfile_path = None

        #The python2.7 version of ConfigParser.RawConfigParser.read() method would silenty ignore a missing config
        #file and return an empty config list.  Not sure if that is true for python3 configparser.ConfigParser.read()
        config = parser.read(fpath_config)

        if len(config) > 0:
            initial_geometry_string = parser.get('Main', 'Geometry')
            initial_scale_factor = parser.getfloat('Main', 'Scale_factor')
            initial_slideshow_dwelltime = parser.getfloat('Main', 'Dwell_time')
            logfile_path = parser.get('Main', 'Logfile')

        this_config = Config(initial_geometry=initial_geometry_string,
                             initial_scale_factor=initial_scale_factor,
                             initial_slideshow_dwelltime = initial_slideshow_dwelltime,
                             logfile_path = logfile_path)

        return(this_config)

    def configure(self, image_fpaths=None, gui_geometry=None, scale_factor=None, dwell_time=None, logfile_path=None):

        _DEFAULT_SCALE_FACTOR = 1.
        _DEFAULT_DWELL_TIME = 0.5
        _DEFAULT_GUI_GEOMETRY = "1200x800"
        _DEFAULT_LOGFILE_PATH = 'rimrocs.log'

        if image_fpaths is not None:
            self.image_fpaths = image_fpaths

        if scale_factor is None:
            scale_factor = _DEFAULT_SCALE_FACTOR
        self.image_scale_factor = scale_factor
        self.scale_size.set(self.image_scale_factor)

        if gui_geometry is None:
            gui_geometry = _DEFAULT_GUI_GEOMETRY
        self.root.geometry(gui_geometry)

        if dwell_time is None:
            dwell_time = _DEFAULT_DWELL_TIME
        self.dwell_time = dwell_time
        self.scale_dwell.set(self.dwell_time)

        if logfile_path is None:
            logfile_path = _DEFAULT_LOGFILE_PATH
        if self.log_fptr is None:
            if os.path.exists(logfile_path):
                self.log_fptr = open(logfile_path, 'a')
            else:
                self.log_fptr = open(logfile_path, 'w')
        else:
            #Need to raise an exception
            sys.stderr.write('In object ImgDisplay.configure(), a file pointer to a logfile is already open.\n')
        return

    def run(self):
        self.load()
        self.display()
        self.root.mainloop()

    def load(self):
        #Load image and data from source
        image = PIL.Image.open(self.image_fpaths[self.image_index])
        self.image_format = image.format
        self.image_size_orig = (image.size[0], image.size[1])
        self.working_image = image
        self.display()
        #self.displayed_image = manipulate.scale_image(self.working_image, self.image_scale_factor)
        #self.scale_displayed_image()
        #display_size = (int(working_image.size[0]*self.image_scale_factor), int(working_image.size[1]*self.image_scale_factor))
        #self.displayed_image = image.resize(newsize, PIL.Image.ANTIALIAS)

    def reload(self):
        self.load()
        self.display()

    def display(self, seq=None):
        if seq == 'prev':
            if self.image_index == 0:
                self.image_index = len(self.image_fpaths) - 1
            else:
                self.image_index -= 1
            self.load()
        elif seq == 'next':
            if self.image_index == len(self.image_fpaths)-1:
                self.image_index = 0
            else:
                self.image_index += 1
            self.load()


        #Save a reference to the image
        self.displayed_image = manipulate.scale_image(self.working_image, self.image_scale_factor)
        self.tk_image_for_canvas = PIL.ImageTk.PhotoImage(self.displayed_image)
        self.canvas.create_image(0, 0, anchor=tkinter.NW, image=self.tk_image_for_canvas, tag='canvas_image')
        #Update the scroll region to be consistent with the current image
        self.canvas.configure(scrollregion=(0, 0, self.tk_image_for_canvas.width(), self.tk_image_for_canvas.height()))
        title = '%s @ %02d per cent' %(self.image_fpaths[self.image_index],
                                       int(self.image_scale_factor*100.))

        self.root.title(title)
        self.root.update()
        return

    def set_dwell_time(self, dwell_time_string):
        self.dwell_time = float(dwell_time_string)

    
    def rotate_working_image(self):
        if self.image_format == 'JPEG' and self.enable_exiftran:
              self.working_image = manipulate.rotate_image_exiftran(self.working_image)
        else:
              self.working_image = manipulate.rotate_image(self.working_image)
        self.display()

    def crop_working_image(self):
        if self.cropbox is not None:
            self.working_image = manipulate.cropimage(self.working_image, self.cropbox_coords_start, self.cropbox_coordse_end)
            self.working_image = self.working_image.crop((int(self.cropbox_coords_start[0]), int(self.cropbox_coords_start[1]),
                                                         int(self.cropbox_coords_end[0]), int(self.cropbox_coords_end[1])))
            self.canvas.delete(self.cropbox)
            self.cropbox = None
            self.btn_crop.config(state=tkinter.DISABLED)
            #Reposition the canvas view
            self.canvas.xview_moveto(0)
            self.canvas.yview_moveto(0)
            self.display()

    


    def adjust_image_scale(self, scale_factor_string):
        #tkinter sliders return a string representing the slider value
        #Convert to float
        scale_factor = float(scale_factor_string)
        self.image_scale_factor = scale_factor
        self.scale_displayed_image()

    #Scaling is applied only to produce the displayed image from the working image
    def scale_displayed_image(self):
        #Get the working_image sizes
        self.displayed_image = manipulate.scale_image(self.working_image, self.image_scale_factor)
        #Preserve the main window size
        #This prevents the main window size from being changed as the "Scale" slider is dragged
        self.root.pack_propagate(False)
        self.display()
        return



    def quit(self):
        if self.log_fptr is not None:
            self.log_fptr.close()
        self.root.quit()
        self.root.destroy()

    def write_fpath(self, event):
        if self.log_fptr is None:
            sys.stderr.write('In object ImgDisplay.write_fpath(), a logfile is not open.\n')
        else:
           self.log_fptr.write('%s\n' %(self.image_fpaths[self.image_index],))
        return

    def cropbox_start(self, event):
        if self.cropbox is not None:
            self.canvas.delete(self.cropbox)
            self.btn_crop.config(state=tkinter.DISABLED)
        #self.canvas.scan_mark(event.x, event.y)
        self.btn_clear_cropbox.config(state=tkinter.NORMAL)
        self.cropbox_coords_start = [self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)]
        self.cropbox = self.canvas.create_rectangle(self.cropbox_coords_start[0], self.cropbox_coords_start[1],
                                                    self.cropbox_coords_start[0], self.cropbox_coords_start[1],
                                                    width=3, outline='blue')


    def interrupt(self):
        return 

    def cropbox_make(self, event):
        x_current = self.canvas.canvasx(event.x)
        y_current = self.canvas.canvasy(event.y)
        w, h = self.canvas.winfo_width(), self.canvas.winfo_height()
        cropbox_coords = self.canvas.coords(self.cropbox)
        
        if event.x > 0.92*w:
            self.canvas.xview_scroll(1, 'units')
            x_current = self.canvas.canvasx(event.x)
            self.canvas.coords(self.cropbox, self.cropbox_coords_start[0], self.cropbox_coords_start[1], x_current, y_current)
            canvas_x_root_start = self.canvas.canvasx(self.canvas.winfo_rootx())
            canvas_y_root_end = self.canvas.canvasy(self.canvas.winfo_rooty())
            self.root.after(200, self.interrupt)
        elif event.x < 0.07*w:
            self.canvas.xview_scroll(-1, 'units')
            rect_x_start, rect_y_start, rect_x_end, rect_y_end = self.canvas.coords(self.cropbox)
            self.canvas.coords(self.cropbox, self.cropbox_coords_start[0], self.cropbox_coords_start[1], x_current, y_current)
            self.root.after(200, self.interrupt)
        if event.y > 0.92*h:
            self.canvas.yview_scroll(1, 'units')
            y_current = self.canvas.canvasy(event.y)
            self.canvas.coords(self.cropbox, self.cropbox_coords_start[0], self.cropbox_coords_start[1], x_current, y_current)
            self.root.after(200, self.interrupt)
        elif event.y < 0.07*h:
            self.canvas.yview_scroll(-1, 'units')
            y_current = self.canvas.canvasy(event.y)
            self.canvas.coords(self.cropbox, self.cropbox_coords_start[0], self.cropbox_coords_start[1], x_current, y_current)
            self.root.after(200, self.interrupt)
        self.canvas.coords(self.cropbox, self.cropbox_coords_start[0], self.cropbox_coords_start[1], x_current, y_current)

    def cropbox_end(self, event):
        self.cropbox_coords_end = [self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)]
        self.btn_crop.config(state=tkinter.NORMAL)

    def clear_cropbox(self):
        self.cropbox_coords_start = None
        self.cropbox_coords_end = None
        self.canvas.delete(self.cropbox)
        self.cropbox = None
        self.btn_clear_cropbox.config(state=tkinter.DISABLED)
        self.display()

    def convert_coord(self, coord):
        #Convert a coordinate from canvas-space (the scaled image) to working-image space
        transformed_coord = coord/self.image_scale_factor
        return tranformed_coord

    def crop_working_image(self):
        if self.cropbox is not None:
            #Convert canvas coordinates to working image coordinates
            image_coords_start = [int(self.cropbox_coords_start[0]/self.image_scale_factor), int(self.cropbox_coords_start[1]/self.image_scale_factor)]
            image_coords_end = [int(self.cropbox_coords_end[0]/self.image_scale_factor), int(self.cropbox_coords_end[1]/self.image_scale_factor)]
            self.working_image = self.working_image.crop((image_coords_start[0], image_coords_start[1],
                                                          image_coords_end[0], image_coords_end[1]))
            self.canvas.delete(self.cropbox)
            self.cropbox = None
            self.btn_crop.config(state=tkinter.DISABLED)
            #Reposition the canvas view
            self.canvas.xview_moveto(0)
            self.canvas.yview_moveto(0)
            self.display()
 
    def slideshow(self):
        #This either starts or stops the slideshow, depending on the current value
        #of self.slideshow_running
        if self.slideshow_running == False:
            #Start the slideshow
            self.btn_slideshow.config(relief='sunken')
            self.btn_next.config(state=tkinter.DISABLED)
            self.btn_prev.config(state=tkinter.DISABLED)
            self.btn_save.config(state=tkinter.DISABLED)
            self.slideshow_running = True
            while self.slideshow_running == True:
                self.display(seq='next')
                time.sleep(self.dwell_time)
        else:
            #Stop the slideshow
            self.slideshow_running = False
            self.btn_slideshow.config(relief='raised')
            self.btn_next.config(state=tkinter.NORMAL)
            self.btn_prev.config(state=tkinter.NORMAL)
            self.btn_save.config(state=tkinter.NORMAL)


    def save_image(self):
        image_fpath = os.path.abspath(self.image_fpaths[self.image_index])
        fpath_dir, fpath_fname = os.path.split(image_fpath)
        fname_base, fname_ext = os.path.splitext(fpath_fname)
        scale_string = '%02d' %(self.image_scale_factor*100,)
        fname_save = '%s_%s_pct%s' %(fname_base, scale_string, fname_ext)

        fpath_save = tkinter.filedialog.asksaveasfilename(initialfile=fname_save,
                                                    title='Save image as...')

        return_val = None
        if fpath_save is not None and len(fpath_save) != 0:
            try:
                self.displayed_image.save(fpath_save)
                return_val = fpath_save
            except:
                sys.stderr.write('Error saving working image as %s.  Try again.\n' %(fpath_save,))
        return return_val

    def save_image_with_meta(self):
        fpath_save = self.save_image()
        
        if fpath_save is not None:
            #Image was saved successfully, save the sidecar file

            image_meta = meta.ImgMeta()

            #Get metadata from source image
            #At present this sets only the image_meta.image_source_capture_datetime field
            image_fpath = os.path.abspath(self.image_fpaths[self.image_index])
            source_image = PIL.Image.open(image_fpath)
            image_meta.get_meta(source_image)

            # Add some other metadata from the saved image
            tz_utc = datetime.timezone.utc
            creation_datetime_utc = datetime.datetime.fromtimestamp(os.path.getctime(fpath_save), tz=tz_utc)
            source_datetime_utc = datetime.datetime.fromtimestamp(os.path.getctime(image_fpath), tz=tz_utc)
            BLOCK_SIZE = 65536
            hasher = hashlib.sha256()
            hash_type = hasher.name
            with open (image_fpath, 'rb') as f_in:
                fb = f_in.read(BLOCK_SIZE)
                while len(fb) > 0:
                    hasher.update(fb)
                    fb = f_in.read(BLOCK_SIZE)
            source_hash = hasher.hexdigest()

            #Prompt for tags
            tag_string = tkinter.simpledialog.askstring('Tags', 'Enter comma-separated tags')
            if tag_string is not None:
                tags = [x.strip() for x in tag_string.split(',')]
            else:
                tags = [] 
            

            fpath_save_dir, fname_save = os.path.split(fpath_save)
            fname_save_base, fname_save_ext = os.path.splitext(fname_save)
            fname_meta_save = '%s.%s' %(fname_save, 'yim')
            fpath_meta_save = os.path.join(fpath_save_dir, fname_meta_save)
       
            
            image_meta.configure(filename=fname_save,
                                 creation_datetime=creation_datetime_utc,
                                 source_fpath=image_fpath,
                                 source_datetime=source_datetime_utc,
                                 source_checksum=source_hash,
                                 checksum_type=hash_type,
                                 tags=tags)

            with open(fpath_meta_save, 'w') as f_out:
                f_out.write('# vim:  ft=yaml\n')
                f_out.write(image_meta.to_yaml())
             
def main():
    d = ImgDisplay()
    config = d.read_config()
    d.root.geometry("800x800")
    d.configure(image_fpaths=sys.argv[1:],
                gui_geometry=config.initial_geometry,
                dwell_time=config.initial_slideshow_dwelltime,
                scale_factor=config.initial_scale_factor,
                logfile_path=config.logfile_path)
    d.run()

if __name__ == "__main__":
    main()
