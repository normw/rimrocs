#!/usr/bin/env python3

"""Image metadata extraction and management

Allows particular image metadata to be extracted from an image, preserved in an
ImgMeta object, modified, and and/or converted to a YAML-formatted output
stream which could then be written to a file  (sometimes called a 'sidecar
file') alongside a saved image.

The metadata file is named by adding the '.yim' extension (YAML
Image Metadata) to the filename used to save the current image.  During the
action, the user will also be prompted to enter a comma-separated list of tags
to describe the image.  The tags will be included in the metadata file.

Library as well as the package Tkinter that provides python bindings for the
Tk gui toolkit.  For saving metadata files with the images, the PyYAML package
is required, along with the other packages listed in the 'if yaml_found' code
block.

Copyright (c) 2020 Norman Wood. All rights reserved.
normw013@fastmail.fm

SPDX-License-Identifier:  BSD-3-Clause
https://spdx.org/licenses/BSD-3-Clause.html#licenseText
"""

import subprocess

try:
   import yaml
except ModuleNotFoundError:
   _ENABLE_META = False
else:
   import datetime
   import hashlib
   _ENABLE_META = True
   #Needed to use OrderedDict and preserve dictionary order on yaml.dump
   from collections import OrderedDict

   def represent_dictionary_order(self, dict_data):
       return self.represent_mapping('tag:yaml.org,2002.map', dict_data.items())

   def setup_yaml():
       yaml.add_representer(OrderedDict, represent_dictionary_order)

   setup_yaml()


class ImgMeta():

    def __init__(self):
        self.image_fname = None
        self.image_creation_datetime = None
        self.image_source_fpath= None
        self.image_source_datetime = None
        self.image_source_capture_datetime = None
        self.image_source_checksum = None
        self.checksum_type = None
        self.image_tags = []

    def load(self, source_yaml):
        #Load an existing yaml metadata file into an ImgMeta instance
        with open(source_yaml, 'r') as f_in:
            metadata_dict = yaml.load(f_in)
        self.image_fname = metadata_dict['filename']
        self.image_creation_datetime = metadata_dict['creation_datetime']
        self.image_source_fpath = metadata_dict['source_filepath']
        self.image_source_datetime = metadata_dict['source_datetime']
        self.image_source_capture_datetime = metadata_dict['source_capture_datetime']
        self.image_source_checksum = metadata_dict['source_checksum']
        self.checksum_type = metadata_dict['checksum_type']
        self.image_tags = metadata_dict['tags']

    def configure(self, filename=None,
                        creation_datetime=None,
                        source_fpath=None,
                        source_datetime=None,
                        source_capture_datetime=None,
                        source_checksum=None,
                        checksum_type=None,
                        tags=None):
        if filename is not None:
            self.image_fname = filename
        if creation_datetime is not None:
            self.image_creation_datetime = creation_datetime
        if source_fpath is not None:
            self.image_source_fpath = source_fpath
        if source_datetime is not None:
            self.image_source_datetime = source_datetime
        if source_capture_datetime is not None:
            self.image_source_capture_datetime = source_capture_datetime
        if source_checksum is not None:
            self.image_source_checksum = source_checksum
        if checksum_type is not None:
            self.checksum_type = checksum_type
        if tags is not None:
            self.image_tags = tags

    def to_yaml(self):
        #Generate YAML output for metadata file
        #metadata_dict = {}
        metadata_dict = OrderedDict()
        metadata_dict['filename'] = self.image_fname
        metadata_dict['creation_datetime'] = self.image_creation_datetime
        metadata_dict['source_filepath'] = self.image_source_fpath
        metadata_dict['source_datetime'] = self.image_source_datetime
        metadata_dict['source_capture_datetime'] = self.image_source_capture_datetime
        metadata_dict['source_checksum'] = self.image_source_checksum
        metadata_dict['checksum_type'] = self.checksum_type
        metadata_dict['tags'] = self.image_tags

        yaml_stream = yaml.dump(metadata_dict)
        return yaml_stream


    def get_meta(self, image):
        EXIF_GROUPS = ['ExifIFD', 'IFD0', 'IFD1']
        KEYS_CAPTURE_DATETIME = ['DateTimeOriginal', 'Date/Time Original']

        meta_info = self._extract_meta(image)
        source_capture_datetime = None

        #Check if there's information in the metadata
        if meta_info is not None:
           if any(x in EXIF_GROUPS for x in meta_info):
               for group in EXIF_GROUPS:
                  if group in meta_info:
                      #Use one of those odd Python "for" loops with an "else" statement
                      #to enable breaking out of both of these nested for loops.
                      for key in KEYS_CAPTURE_DATETIME:
                          if key in meta_info[group]:
                              source_capture_datetime = meta_info[group][key]
                              break
                      else:
                          continue
                      break
           elif any(x in KEYS_CAPTURE_DATETIME for x in meta_info):
              #Try it without without the group
              for key in KEYS_CAPTURE_DATETIME:
                  if key in meta_info:
                      source_capture_datetime = meta_info[key]
                      break

           else:
              #Maybe try to get it from the filename
              #pass for now
              pass

        self.image_source_capture_datetime = source_capture_datetime

        
    def _extract_meta(self, image):

        #We're trying to get the capture date from the original image's Exif data
        #Get the Exif data from the original if available and handle error conditions
        #Use ExifTool for now, seems more robust than PIL.Image._getexif()

        meta_info = None
        the_command = ['exiftool', '-a', '-u', '-g1', '%s' %(image.filename,)]
        try:
            p = subprocess.Popen(the_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except OSError as e:
            #If e.errno == 2, this is a "No such file or directory" error for exiftool
            p = None

        if p is not None:
            stdoutdata, stderrdata = p.communicate()
            meta_info = self._parse_exiftool(stdoutdata) 

        return meta_info

        #Here's how it would be done with PIL.Image._getexif()
        #try:
        #    exif_getter = image._getexif()
        #except ZeroDivisionError:
        #    #Some versions of _getexif() produce a zero division error if the image has
        #    #problematic or nonexistent Exif data
        #    sys.stderr.write('Source image at %s has invalid Exif data.\n' %(image_fpath,))
        #    exif_getter = None

        #if exif_getter is not None:
        #    meta_info = {
        #        PIL.ExifTags.TAGS[k]: v
        #        for k, v in exif_getter.items()
        #        if k in PIL.ExifTags.TAGS
        #    }
            

    def _parse_exiftool(self, datastring):
        #Parse output of ExifTool into a dictionary
        data = {}
        datalines = datastring.splitlines()
        for dataline in datalines:
            if dataline[0:4] == b'----':
                group_tag = dataline.strip('- ')
                data[group_tag] = {}
            else:
                key, value = map(lambda x: x.strip(), dataline.split(b':', 1))
                data[group_tag][key] = value

        return data          
