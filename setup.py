from setuptools import setup, find_packages

with open('README.rst', 'rb') as f:
    long_descr = f.read().decode('utf-8')

setup(name='RImRoCS',
    packages=find_packages('.', exclude=['test', 'tests']),
    package_dir={'':'.'},
    description = 'A TkInter-based GUI application for simple image manipulation and display',
    entry_points = {
        'console_scripts': ['rimrocs=rimrocs.rimrocs:main']
        },
    version='0.1',
    long_description=long_descr,
    author = 'Norm Wood',
    author_email = 'normw013@fastmail.fm',
    license = 'BSD-3-Clause',
    zip_safe = False,
    include_package_data = True,
   )
