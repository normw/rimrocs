RImRoCS:  Raster Image Rotate-Crop-Scale
########################################

A TkInter-based GUI tool for displaying and editing (rotate, crop, scale)
raster images.  It also includes a slideshow function.  It uses the Python
Imaging Library (Pillow) and exiftran if available to display and modify
images.  When saving the modified image, it can also save a YAML-formatted
file (sometimes called a sidecar file)  containing basic image metadata
and information about the source image.


The tool displays images of the list of image files passed on the command
line.  If the package is installed (e.g. using setuptools or pip), an
executable called 'rimrocs' will be installed.  Call this executable with
a list of image files, e.g., 

.. code:: bash

   $ rimrocs ./\*.png

Alternately, call the script directly with a list of images:

.. code:: bash

   $ rimrocs.py ./\*.png

Clicking 'Next' and 'Previous' will cycle through the images, wrapping around
when the beginning or end of the list of images is reached.

Clicking 'Quit' will exit the application.

Clicking 'Slideshow' will start a slideshow.  The delay between images is set
via the 'Slideshow delay' slider to the right of the image.  Clicking 'Slideshow'
again will stop the slideshow at the current image.

The image display can be scaled in size using the 'Scale' slider to the right
of the image.  Note that if you scale the image larger or smaller than the
window, the window size doesn't adjust - you can drag the corners if needed.

The current image can be saved at its current size using the 'Save' button.  By
default, the image will be saved in the current directory using a filename that
contains the original image filename as well as the current size scale in per
cent.  Also by default, the image is saved using the same extension and image
format as the original image.  A dialog will appear that allows this name to be
changed.

The current image can also be saved using the 'Save w/ Meta' button.  This
action saves the current image along with a metadata file that contains
information to help identify the original source image from which the current
image was created.  Such a metadata file is sometimes called a 'sidecar file'.
The button is enabled only if the Python YAML bindings module PyYAML is
installed.  The metadata file is named by adding the '.yim' extension (YAML
Image Metadata) to the filename used to save the current image.  During the
action, the user will also be prompted to enter a comma-separated list of tags
to describe the image.  The tags will be included in the metadata file.

A source image filename can be recorded, allowing the user to pick images for
further review.  To record the filename, just right-click over the image.  The
filename will be saved in a file specified by the log_fpath argument to the
configure() function.  Repeated clicks will append additional filenames to this
file.  By default, the file is named 'image_filename_log.txt'

The script requires the packages Image and ImageTk from the Python Imaging
Library as well as the package Tkinter that provides python bindings for the
Tk gui toolkit.  For saving metadata files with the images, the PyYAML package
is required, along with the other packages listed in the 'if yaml_found' code
block.

Image rotating and cropping are done on a working image.  This working image is
originally a copy of the source image, but it is updated as rotations and
cropping are applied.  Rotations are done with as minimal of loss as possible,
meaning that for JPEG files, either exiftran is used if available, or PILLOW's
rotation is applied to a copy of the working image created with 100% quality
and zero chroma resampling.  Since image scaling is a lossy operation, scaling
is used only to produce an image for display.  The current scale factor is
applied to the working image to produce this display image, so any rotations or
cropping are preserved.  If the image is saved, it is the scaled version of the
working image that is saved.

Clicking 'Reload' will reload the working image from the source, losing any
rotations and cropping, and the restored working image will be displayed using
the current scale factor.  The same is true if the 'Next' or 'Previous' buttons
are used to move to another image.

This package consists of open-source software.  Copyright (c) 2020 Norman Wood.
All rights reserved.  It is distributed under terms of the BSD-3-Clause license:

SPDX-License-Identifier:  BSD-3-Clause
https://spdx.org/licenses/BSD-3-Clause.html#licenseText

a copy of which is provided in the file LICENSE that accompanies this package.
